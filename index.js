const server = require("http").createServer();
const io = require("socket.io")(server, {
  cors: {
    origin: "*",
  },
});

const PORT = 3030;
const NEW_CHAT_MESSAGE_EVENT = "newChatMessage";
const TYPING = "typing";
const NOT_TYPING = "notTyping";

io.on("connection", (socket) => {
  // Join a conversation
  const { roomId } = socket.handshake.query;
  socket.join(roomId);

  // Listen for new messages
  socket.on(NEW_CHAT_MESSAGE_EVENT, (data) => {
    io.in(roomId).emit(NEW_CHAT_MESSAGE_EVENT, data);
  });

  // Listen for typing messages
  socket.on(TYPING, (data) => {
    socket.broadcast.to(roomId).emit(TYPING, data);
    // io.in(roomId).emit(TYPING, data);
  });

  // Listen for stopped typing messages
  socket.on(NOT_TYPING, (data) => {
    io.in(roomId).emit(NOT_TYPING, data);
  });

  // Leave the room if the user closes the socket
  socket.on("disconnect", () => {
    socket.leave(roomId);
  });
});

server.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
});
